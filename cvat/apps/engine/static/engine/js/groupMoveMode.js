class groupMoveMode {

    constructor() {
        this.active = null;
        this.frameContent = $('#frameContent');
        this.clickPoint = null;
        this.selection = null;
        this.actionType = null;
        this.selectedPoints = [];
    }

    init() {
        $(window).on('keydown.pointshift', (event) => {
            if (event.keyCode === 16 && !this.active) {
                this.activate();
            }
        });

        $(window).on('keyup.pointshift', (event) => {
            if (event.keyCode === 16) {
                this.deactivate();
            }
        });
    }

    activate() {
        this.active = true;
        window.cvat.mode = 'groupping';
        this.addDrawHandlers();
    }

    deactivate() {
        this.saveSelectedPointsCoords();// save before deactivete
        this.active = false;
        window.cvat.mode = null;
        this.clickPoint = null;
        this.actionType = null;
        this.selectedPoints = [];
        this.removeDrawHandlers();
        this.removeSelection();
    }

    addDrawHandlers() {
        function stopResizing() {
            if (this.actionType === 'draw_selection') {
                this.actionType = null;
                this.selectPoints();
            }
        }
        this.frameContent.on('mousedown.draw_selection', (e) => {
            this.setClickPoint(e);
            if (this.actionType === null) {
                this.actionType = 'draw_selection';
                this.addSelection();
            }
        });
        this.frameContent.on('mousemove.draw_selection', (e) => {
            if (this.actionType === 'draw_selection') {
                this.resizeSelection(e);
            }
        });
        this.frameContent.on('mouseup.draw_selection', (e) => {
            stopResizing.call(this);
        });
        this.frameContent.on('mouseleave.draw_selection', (e) => {
            stopResizing.call(this);
        });
    }

    removeDrawHandlers() {
        this.frameContent.off('mousedown.draw_selection');
        this.frameContent.off('mousemove.draw_selection');
        this.frameContent.off('mouseup.draw_selection');
        this.frameContent.off('mouseleave.draw_selection');
    }

    selectPoints() {
        this.selectedPoints = [];
        for (let shape of this.frameContent.find('.shape')) {
            let bbox = shape.getBBox(),
                point = [bbox.x, bbox.y];
            if (this.pointInsidePolygon(point, this.selection.points)) {
                this.selectedPoints.push({
                    node: shape,
                    cx: bbox.x,
                    cy: bbox.y
                });
            }
        }
    }

    addSelection() {
        this.removeSelection();
        let polygon = $(document.createElementNS('http://www.w3.org/2000/svg', 'polygon')),
            circles = [];
        polygon.attr({
            'stroke-width': 1,
            'stroke': 'green',
            'fill': 'green',
            'fill-opacity': 0.5,
            'stroke-dasharray': 5,
            'style': 'cursor: move;'
        });
        polygon.appendTo(this.frameContent);

        let strokeWidth = 3,
            radius = 7;
        for (let i = 0; i < 4; i++) {
            let circle = $(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
            circle.attr({
                'stroke-width': strokeWidth,
                'stroke': 'black',
                'r': radius,
                'fill': 'white',
                'fill-opacity': 1,
                'style': 'cursor: ne-resize;'
            });
            circle.appendTo(this.frameContent);
            circles.push({
                node: circle,
                radius: radius
            });
            $(circle).on('mousedown.rotation_circle_activate', function (e) {
                this.selection.activeCircle = i;
            }.bind(this));
        }

        this.selection = {
            polygon: polygon,
            circles: circles,
            deg: 0
        };

        this.addMoveHandlers();
        this.addRotationHandlers();
    }

    removeSelection() {
        if (this.selection) {
            this.removeMoveHandlers();
            this.removeRotationHandlers();
            this.selection.polygon.remove();
            this.selection.circles.map(circle => {
                circle.node.remove();
            });
            this.selection = null;
        }
    }

    resizeSelection(e) {
        let currentPoint = this.getCurrentPoint(e),
            x = Math.min(this.clickPoint.x, currentPoint.x),
            y = Math.min(this.clickPoint.y, currentPoint.y),
            width = Math.max(this.clickPoint.x, currentPoint.x) - Math.min(this.clickPoint.x, currentPoint.x),
            height = Math.max(this.clickPoint.y, currentPoint.y) - Math.min(this.clickPoint.y, currentPoint.y),
            points = [],
            polygon_poins = [];
        for (let i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    points.push([x, y]);
                    break;
                case 1:
                    points.push([x + width, y]);
                    break;
                case 2:
                    points.push([x + width, y + height]);
                    break;
                case 3:
                    points.push([x, y + height]);
                    break;
            }
            polygon_poins.push(`${points[i].join(',')}`);
            this.selection.circles[i].node.attr({
                'cx': points[i][0],
                'cy': points[i][1]
            });
            this.selection.circles[i].cx = points[i][0];
            this.selection.circles[i].cy = points[i][1];
        }
        this.selection.polygon.attr({
            'points': polygon_poins.join(' ')
        });
        this.selection.points = points;
        let center = this.findPolygonCenter(points);
        this.selection.cx = center.x;
        this.selection.cy = center.y;

        this.selection.circles.map(circle => {
            circle.deg = this.getCircleDeg(circle.cx, circle.cy);
        });
    }

    addMoveHandlers() {
        function stopMoving() {
            if (this.actionType === 'move_selection') {
                this.actionType = null;
                this.saveSelectionCoords();
                this.saveSelectedPointsCoords();
            }
        }
        this.selection.polygon.on('mousedown.move_selection', (e) => {
            this.actionType = 'move_selection';
            this.setClickPoint(e);
        });
        this.frameContent.on('mousemove.move_selection', (e) => {
            if (this.actionType === 'move_selection') {
                this.moveSelection(e);
            }
        });
        this.frameContent.on('mouseup.move_selection', (e) => {
            stopMoving.call(this);
        });
        this.frameContent.on('mouseleave.move_selection', (e) => {
            stopMoving.call(this);
        });
    }

    removeMoveHandlers() {
        this.selection.polygon.off('mousedown.move_selection');
        this.frameContent.off('mousemove.move_selection');
        this.frameContent.off('mouseup.move_selection');
        this.frameContent.off('mouseleave.move_selection');
    }


    getMoveDelta(e) {
        let currentPoint = this.getCurrentPoint(e),
            dx = currentPoint.x - this.clickPoint.x,
            dy = currentPoint.y - this.clickPoint.y;
        return {
            x: dx,
            y: dy
        };
    }

    moveSelection(e) {
        let delta = this.getMoveDelta(e);

        this.updateSelectionCoords(point => {
            point[0] += delta.x;
            point[1] += delta.y;
            return point;
        });

        this.updateSelectedCoords( (x,y) => {
            return {
                x: x + delta.x,
                y: y + delta.y
            }
        });
    }

    addRotationHandlers() {
        function stopRotation() {
            if (this.actionType === 'rotate_selection') {
                this.actionType = null;
                this.saveSelectionCoords();
                this.saveSelectedPointsCoords();
            }
        }
        this.selection.circles.map(circle => {
            circle.node.on('mousedown.rotate_selection', (e) => {
                this.setClickPoint(e);
                this.actionType = 'rotate_selection';
            });
        });
        this.frameContent.on('mousemove.rotate_selection', (e) => {
            if (this.actionType === 'rotate_selection') {
                this.rotateSelection(e);
            }
        });
        this.frameContent.on('mouseup.rotate_selection', (e) => {
            stopRotation.call(this);
        });
        this.frameContent.on('mouseleave.rotate_selection', (e) => {
            stopRotation.call(this);
        });
    }

    removeRotationHandlers() {
        this.selection.circles.map(circle => {
            circle.node.off('mousedown.rotate_selection');
            circle.node.off('mousedown.rotation_circle_activate');
        });
        this.frameContent.off('mousemove.rotate_selection');
        this.frameContent.off('mouseup.rotate_selection');
        this.frameContent.off('mouseleave.rotate_selection');
    }

    getRotationDeg(clientX, clientY) {
        let clientCenter = this.canvasToClient(this.selection.cx, this.selection.cy),
            phi = Math.atan2(clientX - clientCenter.x, clientY - clientCenter.y),
            deg = (phi * (180 / Math.PI) * -1) + 180;
        return deg;
    }

    rotateSelection(e) {
        let deg = this.getRotationDeg(e.clientX, e.clientY),
            degFix = this.selection.circles[this.selection.activeCircle].deg;
        deg -= degFix;
        this.selection.deg = deg;

        this.updateSelectionCoords( point => {
            return this.rotatePoint(point, deg);
        });

        this.updateSelectedCoords((x, y) => {
            let rotated = this.rotatePoint([x, y], deg);
            return {
                x: rotated[0],
                y: rotated[1]
            };
        });
    }

    updateSelectionCoords(updateFunc) {
        let points = JSON.parse(JSON.stringify(this.selection.points)),
            polygon_points = [];
        points.map((point, index) => {
            point = updateFunc(point);
            polygon_points.push(point.join(','));
            let circle = this.selection.circles[index];
            circle.node.attr({
                cx: point[0],
                cy: point[1]
            });
        });
        this.selection.polygon.attr({
            points: polygon_points.join(' ')
        });
    }

    updateSelectedCoords(updateFunc){
        this.selectedPoints.map(shape => {
            let cv = shape.node.cvatView,
                shapeType = cv._controller.type;
            if (shapeType === 'annotation_points' || shapeType === 'interpolation_points') {
                let group = cv._uis.points.node,
                    transform = group.getAttribute('transform'),
                    tx = 0,
                    ty = 0;
                if (transform) {//добавляем transform группы к координатам круга 
                    transform = transform.split(',');
                    tx = parseFloat(transform[4]);
                    ty = parseFloat(transform[5]);
                    group.removeAttribute('transform');
                }
                let circle = group.firstElementChild,
                    newCoords = updateFunc(shape.cx + tx, shape.cy + ty)
                $(circle).attr({
                    cx: newCoords.x,
                    cy: newCoords.y
                });
            }
        });
    }

    saveSelectionCoords() {
        let polygon_points = this.selection.polygon.attr('points').split(' '),
            points = [];
        polygon_points.map((p_point, index) => {
            let point = p_point.split(','),
                x = parseFloat(point[0]),
                y = parseFloat(point[1]);
            points.push([x, y]);

            let circle = this.selection.circles[index];
            circle.cx = x;
            circle.cy = y;
        });
        this.selection.points = points;

        let center = this.findPolygonCenter(points);
        this.selection.cx = center.x;
        this.selection.cy = center.y;

        this.selection.circles.map(circle => {
            circle.deg = this.getCircleDeg(circle.cx, circle.cy);
        });
    }

    fixZOrder(){
        this.frameContent.append(this.selection.polygon);
        this.selection.circles.map( circle => {
            this.frameContent.append(circle.node);
        });
    }

    saveSelectedPointsCoords() {
        this.selectedPoints.map(shape => {
            let cv = shape.node.cvatView,
                shapeType = cv._controller.type;
            if (shapeType === 'annotation_points' || shapeType === 'interpolation_points') {
                let circle = cv._uis.points.node.firstElementChild,
                    cx = parseFloat(circle.getAttribute('cx')),
                    cy = parseFloat(circle.getAttribute('cy'));
                shape.node.setAttribute( 'points', [cx, cy].join(',') );
                const frame = window.cvat.player.frames.current;
                shape.node.cvatView._controller.updatePosition(frame, shape.node.cvatView._buildPosition());
                this.selectPoints(); //select updated points
                this.fixZOrder();
            }
        });
    }

    pointInsidePolygon(point, polygon) {
        var x = point[0], y = point[1];
        var inside = false;
        for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
            var xi = polygon[i][0], yi = polygon[i][1];
            var xj = polygon[j][0], yj = polygon[j][1];

            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }
        return inside;
    }

    rotatePoint(point, angle) {
        let cx = this.selection.cx,
            cy = this.selection.cy,
            x = point[0],
            y = point[1],
            radians = (Math.PI / 180) * (360 - angle),
            cos = Math.cos(radians),
            sin = Math.sin(radians),
            nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
            ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
        return [nx, ny];
    }

    findPolygonCenter(points) {
        let x_points = [],
            y_points = [];
        points.map(point => {
            x_points.push(point[0]);
            y_points.push(point[1]);
        });
        let min_x = Math.min(...x_points),
            max_x = Math.max(...x_points),
            min_y = Math.min(...y_points),
            max_y = Math.max(...y_points);
        return {
            x: min_x + (max_x - min_x) / 2,
            y: min_y + (max_y - min_y) / 2
        }
    }

    getCircleDeg(x, y) {
        let clientCircle = this.canvasToClient(x, y);
        return this.getRotationDeg(clientCircle.x, clientCircle.y);
    }

    setClickPoint(e) {
        this.clickPoint = this.getCurrentPoint(e);
    }

    getCurrentPoint(e) {
        return window.cvat.translate.point.clientToCanvas(this.frameContent[0], e.clientX, e.clientY);
    }

    canvasToClient(x, y) {
        return window.cvat.translate.point.canvasToClient(this.frameContent[0], x, y);
    }

    clientToCanvas(x, y) {
        return window.cvat.translate.point.clientToCanvas(this.frameContent[0], x, y);
    }

}